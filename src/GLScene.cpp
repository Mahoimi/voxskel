#include <glm/gtc/type_ptr.hpp> 
#include <ctime>
#include "GLScene.h"

GLScene::GLScene() {
    // Create shapes
    m_mesh.load("../scenes/crytek-sponza.obj");

    initVoxelization();
    //createVoxelGrid();
    createCubicalComplex();
    createGLCubicalComplex();
}

void GLScene::initVoxelization(){
    // Calculate voxelization render time
    clock_t timer = clock();

    // Voxelize the mesh
    m_voxelizer.voxelize(128, m_mesh);

    std::cout << "Voxelization render time (seconds) = "<< ((float)(clock() - timer))/CLOCKS_PER_SEC << std::endl;
}

void GLScene::createCubicalComplex(){
    clock_t timer = clock();

    m_cubicalComplex = m_voxelizer.createCubicalComplex();

    std::cout << "createCubicalComplex time (in seconds) = " << ((float)(clock() - timer))/CLOCKS_PER_SEC << std::endl;

    timer = clock();

    m_cubicalComplex.calculateFirstBorder();

    std::cout << "calculateFirstBorder time (in seconds) = " << ((float)(clock() - timer))/CLOCKS_PER_SEC << std::endl;
}

void GLScene::createGLCubicalComplex(){
    clock_t timer = clock();

    m_glCubicComplex.init(m_cubicalComplex);

    //std::swap(m_glCubicComplex.m_CCTextureObject, m_voxelizer.m_VoxelGridTextureObject);

    std::cout << "GLCubicalComplex creation time (in seconds) = " << ((float)(clock() - timer))/CLOCKS_PER_SEC << std::endl;
}

void GLScene::skeletonization(int iter) {

    //std::cout << "Connexity number = " << m_cubicalComplex.computeConnexityNumber() << std::endl;
    std::cout << "Ki = " << m_cubicalComplex.computeEulerCharacteristic() << std::endl;

    clock_t timer = clock();

    m_cubicalComplex.parDirCollapse(iter);

    std::cout << "Skeletonization time (in seconds) = " << ((float)(clock() - timer)) / CLOCKS_PER_SEC << std::endl;

    //std::cout << "Connexity number = " << m_cubicalComplex.computeConnexityNumber() << std::endl;
    std::cout << "Ki = " << m_cubicalComplex.computeEulerCharacteristic() << std::endl;

    createGLCubicalComplex();
}

void GLScene::skeletonization() {
    //std::cout << "Connexity number = " << m_cubicalComplex.computeConnexityNumber() << std::endl;
    std::cout << "Ki = " << m_cubicalComplex.computeEulerCharacteristic() << std::endl;

    clock_t timer = clock();

    m_cubicalComplex.parDirCollapse();

    std::cout << "Skeletonization time (in seconds) = " << ((float)(clock() - timer)) / CLOCKS_PER_SEC << std::endl;

    //std::cout << "Connexity number = " << m_cubicalComplex.computeConnexityNumber() << std::endl;
    std::cout << "Ki = " << m_cubicalComplex.computeEulerCharacteristic() << std::endl;

    createGLCubicalComplex();
}
