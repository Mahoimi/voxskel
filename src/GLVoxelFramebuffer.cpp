#include "GLVoxelFramebuffer.h"

GLVoxelFramebuffer::GLVoxelFramebuffer(): m_FBO(0), m_renderedTexture(nullptr){
    //Init the Framebuffer Object
    glGenFramebuffers(1, &m_FBO);
}

bool GLVoxelFramebuffer::init(int width, int height, int numTextures){
    delete [] m_renderedTexture;

    //bind the Framebuffer Object
    glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

    m_height = height;
    m_width = width;
    m_numTextures = numTextures;
    m_renderedTexture = new GLuint[numTextures];

    //Init & bind the rendered texture
    glGenTextures(numTextures, m_renderedTexture);

    for(int i = 0; i < numTextures; ++i){
        glBindTexture(GL_TEXTURE_2D, m_renderedTexture[i]);

        // Give an empty image to OpenGL
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32I, width, height, 0, GL_RGBA_INTEGER, GL_INT, 0);

        // Poor filtering. Needed !
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        // Set "renderedTexture" as our colour attachement #0
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, m_renderedTexture[i], 0);
    }

    // Check that our framebuffer is ok
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        return false;
    }

    return true;
}

GLVoxelFramebuffer::~GLVoxelFramebuffer(){
    glDeleteTextures(m_numTextures,m_renderedTexture);
    glDeleteFramebuffers(1,&m_FBO);
    delete [] m_renderedTexture;
}
