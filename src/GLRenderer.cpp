#include "GLRenderer.hpp"

void GLRenderer::drawScene(const GLScene& scene) {
    glm::mat4 MVP = m_ProjectionMatrix * m_ViewMatrix;

    glEnable(GL_DEPTH_TEST);

    m_RenderScenePass.m_Program.use();

    glUniformMatrix4fv(m_RenderScenePass.m_uMVLocation, 1,
                       GL_FALSE, glm::value_ptr(m_ViewMatrix));
    glUniformMatrix4fv(m_RenderScenePass.m_uMVPLocation, 1,
                       GL_FALSE, glm::value_ptr(MVP));

    scene.draw();
}

void GLRenderer::drawCubicalComplex(const glm::mat4& gridToWorld,
                                    const GLCubicalComplex& cubicalComplex) {
    glm::mat4 MVP = m_ProjectionMatrix * m_ViewMatrix * gridToWorld;

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_PROGRAM_POINT_SIZE);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, cubicalComplex.getTextureObject());

    m_RenderCubicalComplexFacesPass.m_Program.use();

    glUniformMatrix4fv(m_RenderCubicalComplexFacesPass.m_uMVPLocation, 1,
                       GL_FALSE, glm::value_ptr(MVP));
    glUniform1i(m_RenderCubicalComplexFacesPass.m_uCubicalComplexLocation, 0);

    cubicalComplex.render();

    m_RenderCubicalComplexEdgesPass.m_Program.use();

    glUniformMatrix4fv(m_RenderCubicalComplexEdgesPass.m_uMVPLocation, 1,
                       GL_FALSE, glm::value_ptr(MVP));
    glUniform1i(m_RenderCubicalComplexEdgesPass.m_uCubicalComplexLocation, 0);

    cubicalComplex.render();

    m_RenderCubicalComplexPointsPass.m_Program.use();

    glUniformMatrix4fv(m_RenderCubicalComplexPointsPass.m_uMVPLocation, 1,
                       GL_FALSE, glm::value_ptr(MVP));
    glUniform1i(m_RenderCubicalComplexPointsPass.m_uCubicalComplexLocation, 0);

    cubicalComplex.render();
}

GLRenderer::RenderScenePass::RenderScenePass() {
   m_Program.load("../shaders/VertexShader.glsl", "../shaders/FragmentShader.glsl");

   m_Program.use();

   m_uMVPLocation = m_Program.getUniformLocation("MVP");
   m_uMVLocation = m_Program.getUniformLocation("MV");
}

GLRenderer::RenderCubicalComplexFacesPass::RenderCubicalComplexFacesPass() {
    m_Program.load("../shaders/CComplex.vs.glsl", "../shaders/CComplex.fs.glsl", "../shaders/CComplexFaces.gs.glsl");

    m_Program.use();

    m_uMVPLocation = m_Program.getUniformLocation("MVP");
    m_uMVLocation = m_Program.getUniformLocation("MV");
    m_uCubicalComplexLocation = m_Program.getUniformLocation("uCubicalComplex");
}

GLRenderer::RenderCubicalComplexEdgesPass::RenderCubicalComplexEdgesPass() {
    m_Program.load("../shaders/CComplex.vs.glsl", "../shaders/CComplex.fs.glsl", "../shaders/CComplexEdges.gs.glsl");

    m_Program.use();

    m_uMVPLocation = m_Program.getUniformLocation("MVP");
    m_uMVLocation = m_Program.getUniformLocation("MV");
    m_uCubicalComplexLocation = m_Program.getUniformLocation("uCubicalComplex");
}

GLRenderer::RenderCubicalComplexPointsPass::RenderCubicalComplexPointsPass() {
    m_Program.load("../shaders/CComplex.vs.glsl", "../shaders/CComplex.fs.glsl", "../shaders/CComplexPoints.gs.glsl");

    m_Program.use();

    m_uMVPLocation = m_Program.getUniformLocation("MVP");
    m_uMVLocation = m_Program.getUniformLocation("MV");
    m_uCubicalComplexLocation = m_Program.getUniformLocation("uCubicalComplex");
}
