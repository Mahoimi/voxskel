#include <vector>

#include "WindowManager.h"
#include "GLScene.h"
#include "GLRenderer.hpp"
#include "FreeFlyCamera.h"

int main(int argc, char* argv[]){
    int width = 1024;
    int height = 768;

	//Create the window
    WindowManager window(width, height);

    GLRenderer renderer;
    GLScene scene;
    FreeFlyCamera camera(glm::vec3(scene.m_mesh.getCenter().x, scene.m_mesh.getCenter().y, scene.m_mesh.getCenter().z));

	//Rasterize
    window.draw(renderer, scene, camera);

	return EXIT_SUCCESS;
}
