#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include "GLVoxelizer.h"
#include "VoxelBuffer.h"

GLVoxelizer::GLVoxelizer() {
    glGenTextures(1, &m_VoxelGridTextureObject);
}

GLVoxelizer::~GLVoxelizer() {
    glDeleteTextures(1, &m_VoxelGridTextureObject);
}

static GLint getUniformLocation(GLuint program, const char* name) {
    GLint uniformLocation = glGetUniformLocation(program, name);
    if(uniformLocation < 0) {
        std::cerr << name << " not found in program " << program << std::endl;
    }
    return uniformLocation;
}

VoxelSpace GLVoxelizer::voxelize(int res, const Mesh& mesh) {
    glBindTexture(GL_TEXTURE_3D, m_VoxelGridTextureObject);

    std::vector<int32_t> zero(res * res * res, 0);

    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32I, res, res, res, 0,
                 GL_RED_INTEGER, GL_INT, zero.data());
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_3D, 0);

    glBindImageTexture(0, m_VoxelGridTextureObject, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);

    m_res = res;

    // Requiered number of color buffer
    m_numRenderTargets = ceil((double) m_res / 128.0);

    m_AABCLength = mesh.getAABCLength();

    m_voxelLength = m_AABCLength / (float) m_res;

    m_origBBox = mesh.getCenter() - glm::vec3(0.5f * m_AABCLength);

    // Load the shaders
    GLProgram voxShader;
    voxShader.load("../shaders/Voxskel.vs.glsl", "../shaders/Voxskel.fs.glsl", "../shaders/Voxskel.gs.glsl");
    GLuint programID = voxShader.getProgramID();

    // Use the shaders
    glUseProgram(programID);

    // Desactivate depth, cullface
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // Blending activated
    glEnable(GL_COLOR_LOGIC_OP);
    glLogicOp(GL_OR);

    // Init FrameBuffer
    if (!m_frameBuffer.init(m_res, m_res, m_numRenderTargets)){
        std::cerr << "FBO Error" << std::endl;
    }

    //Projection matrix : Adapt the viewport to the size of the mesh
    glm::mat4 P = glm::ortho(-m_AABCLength * 0.5f,
                              m_AABCLength * 0.5f,
                             -m_AABCLength * 0.5f,
                              m_AABCLength * 0.5f,
                              0.f,
                              m_AABCLength);

    glm::vec3 position(mesh.getCenter().x, mesh.getCenter().y, mesh.getCenter().z + 0.5 * mesh.getAABCLength());
    glm::vec3 point(mesh.getCenter().x, mesh.getCenter().y, mesh.getCenter().z);
    glm::mat4 V = glm::lookAt(position, point, glm::vec3(0, 1, 0));

    // Get the MVP Matrix
    glm::mat4 MVP = P * V;

    // Geometry shader uniform
    GLint MVPLocation = getUniformLocation(programID, "MVP");
    glUniformMatrix4fv(MVPLocation, 1, GL_FALSE, glm::value_ptr(MVP));

    GLint halfPixelSizeLocation = getUniformLocation(programID, "halfPixelSize");
    glUniform2fv(halfPixelSizeLocation, 1, glm::value_ptr(glm::vec2(1.f / m_res))); //Value given in the thesis

    //GLint halfVoxelSizeNormalizedLocation = getUniformLocation(programID, "halfVoxelSizeNormalized");
    //glUniform1f(halfVoxelSizeNormalizedLocation, 1.f / (2.f * m_res) ); //Value given in the thesis

    GLint numVoxelsLocation = getUniformLocation(programID, "numVoxels");
    glUniform1i(numVoxelsLocation, m_res);

    // Fragment shader uniforms
    GLint origBBoxLocation = getUniformLocation(programID, "origBBox");

    glUniform3fv(origBBoxLocation, 1, glm::value_ptr(m_origBBox));

    GLint numRenderTargetsLocation = getUniformLocation(programID, "numRenderTargets");
    glUniform1i(numRenderTargetsLocation, m_numRenderTargets);

    GLint voxelSizeLocation = getUniformLocation(programID, "voxelSize");
    glUniform1f(voxelSizeLocation, m_voxelLength);

    GLint uVoxelGridLocation = voxShader.getUniformLocation("uVoxelGrid");
    glUniform1i(uVoxelGridLocation, 0);

    m_frameBuffer.bind(GL_DRAW_FRAMEBUFFER);

    // Set the list of draw buffers.
    GLenum DrawBuffers[m_numRenderTargets];
    for(int i = 0; i < m_numRenderTargets; ++i){
        DrawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
    }
    glDrawBuffers(m_numRenderTargets, DrawBuffers);

    glViewport(0, 0, m_res, m_res);

    // Clear the window
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mesh.render();

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    glLogicOp(GL_COPY);

    VoxelSpace space;
    space.resolution = res;
    space.gridToWorld = glm::scale(glm::translate(glm::mat4(1.f), m_origBBox), glm::vec3(m_voxelLength));
    space.worldToGrid = glm::inverse(space.gridToWorld);

    return space;
}

BnZ::VoxelGrid GLVoxelizer::fillVoxelGrid() {
    BnZ::VoxelGrid voxelGrid(m_res, m_res, m_res);

    // Will be used to store the data of a ColorBuffer
    VoxelBuffer voxelBuffer(m_res, m_numRenderTargets);

    // Read the data in the ColorBuffers of the FrameBuffer
    m_frameBuffer.bind(GL_READ_FRAMEBUFFER);

    //Store the data in the VoxelBuffer
    voxelBuffer.storeData();

    // Convert data in voxel position

    voxelBuffer.getVoxels([&voxelGrid](int x, int y, int z) {
        voxelGrid(x, y, z) = 1;
    });

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

    return voxelGrid;
}

CubicalComplex GLVoxelizer::createCubicalComplex(){
    CubicalComplex cubicalComplex(m_res);
    // Will be used to store the data of a ColorBuffer
    VoxelBuffer voxelBuffer(m_res, m_numRenderTargets);

    // Read the data in the ColorBuffers of the FrameBuffer
    m_frameBuffer.bind(GL_READ_FRAMEBUFFER);

    // Store the data in the VoxelBuffer
    voxelBuffer.storeData();

    // Convert data in voxel position
    voxelBuffer.getEmptySpace([&cubicalComplex, this, &voxelBuffer](int x, int y, int z) {
        cubicalComplex(x, y, z).fill();

        auto value = voxelBuffer(x, y, z);

        if (x == m_res - 1 || value != voxelBuffer(x+1, y, z)) cubicalComplex(x+1, y, z).add(POINT+YEDGE+ZEDGE+YZFACE);

        if (y == m_res - 1 || value != voxelBuffer(x, y+1, z)) cubicalComplex(x, y+1, z).add(POINT+XEDGE+ZEDGE+XZFACE);

        if (z == m_res - 1 || value != voxelBuffer(x, y, z+1)) cubicalComplex(x, y, z+1).add(POINT+XEDGE+YEDGE+XYFACE);

        if (x == m_res - 1 || y == m_res - 1 || value != voxelBuffer(x+1,y+1,z)) cubicalComplex(x+1,y+1,z).add(POINT+ZEDGE);

        if (x == m_res - 1 || z == m_res - 1 || value != voxelBuffer(x+1,y,z+1)) cubicalComplex(x+1,y,z+1).add(POINT+YEDGE);

        if (y == m_res - 1 || z == m_res - 1 || value != voxelBuffer(x,y+1,z+1)) cubicalComplex(x,y+1,z+1).add(POINT+XEDGE);

        if (x == m_res - 1 || y == m_res - 1 || z == m_res - 1 || value != voxelBuffer(x+1,y+1,z+1)) cubicalComplex(x+1,y+1,z+1).add(POINT);
    });

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

    return cubicalComplex;
}
