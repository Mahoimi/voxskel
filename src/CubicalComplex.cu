#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>

#include "CubicalComplex.h"

struct CudaCubicalComplex {
    CubicalComplex::CCElmtType* m_faces;
    int m_width, m_height, m_depth;

    __device__ CubicalComplex::CCElmtType operator ()(int x, int y, int z) const {
        return m_faces[x + y * m_width + z * m_width * m_height];
    }

    __device__ bool has(uint32_t idx, CubicalComplex::CCElmtType d) const {
        return m_faces[idx] & d;
    }

    __device__ bool has(int x, int y, int z, CubicalComplex::CCElmtType d) const {
        return m_faces[x + y * m_width + z * m_width * m_height] & d;
    }

    __device__ bool isFreeXNEG(int x, int y, int z) const;
    __device__ bool isFreeXNEG3D(int x, int y, int z) const;
    __device__ bool isFreeXNEG2D(int x, int y, int z) const;
    __device__ bool isFreeXNEG_XYFACE(int x, int y, int z) const;
    __device__ bool isFreeXNEG_XZFACE(int x, int y, int z) const;
    __device__ bool isFreeXNEG1D(int x, int y, int z) const;

    __device__ bool isFreeXPOS(int x, int y, int z) const;
    __device__ bool isFreeXPOS3D(int x, int y, int z) const;
    __device__ bool isFreeXPOS2D(int x, int y, int z) const;
    __device__ bool isFreeXPOS_XYFACE(int x, int y, int z) const;
    __device__ bool isFreeXPOS_XZFACE(int x, int y, int z) const;
    __device__ bool isFreeXPOS1D(int x, int y, int z) const;

    __device__ bool isFreeYNEG(int x, int y, int z) const;
    __device__ bool isFreeYNEG3D(int x, int y, int z) const;
    __device__ bool isFreeYNEG2D(int x, int y, int z) const;
    __device__ bool isFreeYNEG_XYFACE(int x, int y, int z) const;
    __device__ bool isFreeYNEG_YZFACE(int x, int y, int z) const;
    __device__ bool isFreeYNEG1D(int x, int y, int z) const;

    __device__ bool isFreeYPOS(int x, int y, int z) const;
    __device__ bool isFreeYPOS3D(int x, int y, int z) const;
    __device__ bool isFreeYPOS2D(int x, int y, int z) const;
    __device__ bool isFreeYPOS_XYFACE(int x, int y, int z) const;
    __device__ bool isFreeYPOS_YZFACE(int x, int y, int z) const;
    __device__ bool isFreeYPOS1D(int x, int y, int z) const;

    __device__ bool isFreeZNEG(int x, int y, int z) const;
    __device__ bool isFreeZNEG3D(int x, int y, int z) const;
    __device__ bool isFreeZNEG2D(int x, int y, int z) const;
    __device__ bool isFreeZNEG_XZFACE(int x, int y, int z) const;
    __device__ bool isFreeZNEG_YZFACE(int x, int y, int z) const;
    __device__ bool isFreeZNEG1D(int x, int y, int z) const;

    __device__ bool isFreeZPOS(int x, int y, int z) const;
    __device__ bool isFreeZPOS3D(int x, int y, int z) const;
    __device__ bool isFreeZPOS2D(int x, int y, int z) const;
    __device__ bool isFreeZPOS_XZFACE(int x, int y, int z) const;
    __device__ bool isFreeZPOS_YZFACE(int x, int y, int z) const;
    __device__ bool isFreeZPOS1D(int x, int y, int z) const;
};

__device__ bool CudaCubicalComplex::isFreeXNEG(int x, int y, int z) const{
    return isFreeXNEG1D(x, y, z) || isFreeXNEG2D(x, y, z) || isFreeXNEG3D(x, y, z);
}

__device__ bool CudaCubicalComplex::isFreeXNEG3D(int x, int y, int z) const{
    return x > 0 && has(x - 1, y, z, CUBE) && !has(x, y, z, CUBE);
}

__device__ bool CudaCubicalComplex::isFreeXNEG2D(int x, int y, int z) const{
    return isFreeXNEG_XYFACE(x,y,z) || isFreeXNEG_XZFACE(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeXNEG_XYFACE(int x, int y, int z) const{
    return x > 0 && !has(x, y, z, YZFACE) &&
          has(x - 1, y, z, XYFACE) && !has(x, y, z, XYFACE) && (z == 0 || !has(x, y, z - 1, YZFACE));
}

__device__ bool CudaCubicalComplex::isFreeXNEG_XZFACE(int x, int y, int z) const{
    return x > 0 && !has(x, y, z, YZFACE) &&
          has(x - 1, y, z, XZFACE) && !has(x, y, z, XZFACE) && (y == 0 || !has(x, y - 1, z, YZFACE));
}

__device__ bool CudaCubicalComplex::isFreeXNEG1D(int x, int y, int z) const{
    return x > 0 && !has(x, y, z, YEDGE) && !has(x,y,z, ZEDGE) && !has(x,y,z,XEDGE) && has(x - 1,y,z,XEDGE) &&
            (z == 0 || !has(x, y, z - 1,ZEDGE)) && (y == 0 || !has(x, y - 1, z,YEDGE));
}

__device__ bool CudaCubicalComplex::isFreeXPOS(int x, int y, int z) const{
    return isFreeXPOS1D(x, y, z) || isFreeXPOS2D(x, y, z) || isFreeXPOS3D(x, y, z);
}

__device__ bool CudaCubicalComplex::isFreeXPOS3D(int x, int y, int z) const{
    return x < m_width - 1 && has(x,y,z,CUBE) && (x == 0  || !has(x - 1, y, z,CUBE));
}

__device__ bool CudaCubicalComplex::isFreeXPOS2D(int x, int y, int z) const{
    return isFreeXPOS_XYFACE(x, y, z) || isFreeXPOS_XZFACE(x, y, z);
}

__device__ bool CudaCubicalComplex::isFreeXPOS_XYFACE(int x, int y, int z) const{
    return x < m_width - 1 && !has(x, y, z,YZFACE) &&
          has(x, y, z,XYFACE) && (x == 0 || !has(x - 1, y, z,XYFACE)) && (z == 0 || !has(x, y, z - 1,YZFACE));
}

__device__ bool CudaCubicalComplex::isFreeXPOS_XZFACE(int x, int y, int z) const{
    return x < m_width - 1 && !has(x, y, z,YZFACE) &&
          has(x, y, z,XZFACE) && (x == 0 || !has(x - 1, y, z,XZFACE)) && (y == 0 || !has(x, y - 1, z,YZFACE));
}

__device__ bool CudaCubicalComplex::isFreeXPOS1D(int x, int y, int z) const{
    return x < m_width - 1 && !has(x,y,z,YEDGE) && !has(x,y,z,ZEDGE) && has(x,y,z,XEDGE) &&
            (z == 0 || !has(x, y, z - 1,ZEDGE)) && (y == 0 || !has(x, y - 1, z,YEDGE)) && (x == 0 || !has(x - 1, y, z,XEDGE));
}

__device__ bool CudaCubicalComplex::isFreeYNEG(int x, int y, int z) const{
   return isFreeYNEG1D(x, y, z) || isFreeYNEG2D(x, y, z) || isFreeYNEG3D(x, y, z);
}

__device__ bool CudaCubicalComplex::isFreeYNEG3D(int x, int y, int z) const{
    return y > 0 && has(x , y - 1, z,CUBE) && !has(x, y, z,CUBE);
}

__device__ bool CudaCubicalComplex::isFreeYNEG2D(int x, int y, int z) const{
    return isFreeYNEG_XYFACE(x,y,z) || isFreeYNEG_YZFACE(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeYNEG_XYFACE(int x, int y, int z) const{
    return y > 0 && !has(x, y, z,XZFACE) &&
          has(x, y - 1, z,XYFACE) && !has(x, y, z,XYFACE) && (z == 0 || !has(x, y, z - 1,XZFACE));
}

__device__ bool CudaCubicalComplex::isFreeYNEG_YZFACE(int x, int y, int z) const{
    return y > 0 && !has(x, y, z,XZFACE) &&
          has(x, y - 1, z,YZFACE) && !has(x, y, z,YZFACE) && (x == 0 || !has(x - 1, y, z,XZFACE));
}

__device__ bool CudaCubicalComplex::isFreeYNEG1D(int x, int y, int z) const{
    return y > 0 && !has(x, y, z,XEDGE) && !has(x, y, z,YEDGE) && !has(x, y, z,ZEDGE) && has(x, y - 1, z,YEDGE) &&
            (z == 0 || !has(x, y, z - 1,ZEDGE)) && (x == 0 || !has(x - 1, y, z,XEDGE));
}

__device__ bool CudaCubicalComplex::isFreeYPOS(int x, int y, int z) const{
    return isFreeYPOS3D(x,y,z) || isFreeYPOS2D(x,y,z) || isFreeYPOS1D(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeYPOS3D(int x, int y, int z) const{
    return y < m_height - 1 && has(x,y,z,CUBE) && (y == 0  || !has(x, y - 1, z,CUBE));
}

__device__ bool CudaCubicalComplex::isFreeYPOS2D(int x, int y, int z) const{
    return isFreeYPOS_XYFACE(x,y,z) || isFreeYPOS_YZFACE(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeYPOS_XYFACE(int x, int y, int z) const{
    return y < m_height - 1 && !has(x, y, z,XZFACE) &&
          has(x, y, z,XYFACE) && (y == 0 || !has(x, y - 1, z,XYFACE)) && (z == 0 || !has(x, y, z - 1,XZFACE));
}

__device__ bool CudaCubicalComplex::isFreeYPOS_YZFACE(int x, int y, int z) const{
    return y < m_height - 1 && !has(x, y, z,XZFACE) &&
          has(x, y, z,YZFACE) && (y == 0 || !has(x, y - 1, z,YZFACE)) && (x == 0 || !has(x - 1, y, z,XZFACE));
}

__device__ bool CudaCubicalComplex::isFreeYPOS1D(int x, int y, int z) const{
    return y < m_height - 1 && !has(x,y,z,XEDGE) && !has(x,y,z,ZEDGE) && has(x,y,z,YEDGE) &&
            (z == 0 || !has(x, y, z - 1,ZEDGE)) && (y == 0 || !has(x, y - 1, z,YEDGE)) && (x == 0 || !has(x - 1, y, z,XEDGE));
}

__device__ bool CudaCubicalComplex::isFreeZNEG(int x, int y, int z) const{
    return isFreeZNEG3D(x,y,z) || isFreeZNEG2D(x,y,z) || isFreeZNEG1D(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeZNEG3D(int x, int y, int z) const{
     return z > 0 && has(x , y, z - 1,CUBE) && !has(x, y, z,CUBE);
}

__device__ bool CudaCubicalComplex::isFreeZNEG2D(int x, int y, int z) const{
    return isFreeZNEG_XZFACE(x,y,z) || isFreeZNEG_YZFACE(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeZNEG_XZFACE(int x, int y, int z) const{
    return z > 0 && !has(x, y, z,XYFACE) &&
          has(x, y, z - 1,XZFACE) && !has(x, y, z,XZFACE) && (y == 0 || !has(x, y - 1, z,XYFACE));
}

__device__ bool CudaCubicalComplex::isFreeZNEG_YZFACE(int x, int y, int z) const{
    return z > 0 && !has(x, y, z,XYFACE) &&
          has(x, y, z - 1,YZFACE) && !has(x, y, z,YZFACE) && (x == 0 || !has(x - 1, y, z,XYFACE));
}

__device__ bool CudaCubicalComplex::isFreeZNEG1D(int x, int y, int z) const{
    return z > 0 && !has(x, y, z,XEDGE) && !has(x, y, z,YEDGE) && !has(x, y, z,ZEDGE) && has(x, y, z - 1,ZEDGE) &&
            (y == 0 || !has(x, y - 1, z,YEDGE)) && (x == 0 || !has(x - 1, y, z,XEDGE));
}

__device__ bool CudaCubicalComplex::isFreeZPOS(int x, int y, int z) const{
    return isFreeZPOS3D(x,y,z) || isFreeZPOS2D(x,y,z) || isFreeZPOS1D(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeZPOS3D(int x, int y, int z) const{
    return z < m_height - 1 && has(x,y,z,CUBE) && (z == 0  || !has(x, y, z - 1,CUBE));
}

__device__ bool CudaCubicalComplex::isFreeZPOS2D(int x, int y, int z) const{
    return isFreeZPOS_XZFACE(x,y,z) || isFreeZPOS_YZFACE(x,y,z);
}

__device__ bool CudaCubicalComplex::isFreeZPOS_XZFACE(int x, int y, int z) const{
    return z < m_height - 1 && !has(x, y, z,XYFACE) &&
          has(x, y, z,XZFACE) && (z == 0 || !has(x, y, z - 1,XZFACE)) && (y == 0 || !has(x, y - 1, z,XYFACE));
}

__device__ bool CudaCubicalComplex::isFreeZPOS_YZFACE(int x, int y, int z) const{
    return z < m_height - 1 && !has(x, y, z,XYFACE) &&
          has(x, y, z,YZFACE) && (z == 0 || !has(x, y, z - 1,YZFACE)) && (x == 0 || !has(x - 1, y, z,XYFACE));
}

__device__ bool CudaCubicalComplex::isFreeZPOS1D(int x, int y, int z) const{
    return z < m_height - 1 && !has(x,y,z,XEDGE) && !has(x,y,z,YEDGE) && has(x,y,z,ZEDGE) &&
            (z == 0 || !has(x, y, z - 1,ZEDGE)) && (y == 0 || !has(x, y - 1, z,YEDGE)) && (x == 0 || !has(x - 1, y, z,XEDGE));
}

__global__ void testForBorderKernel(CudaCubicalComplex cc, int* borderIndex) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    int z = blockIdx.z * blockDim.z + threadIdx.z;

    if(x >= cc.m_width || y >= cc.m_height || z >= cc.m_depth) {
        return;
    }

    int idx = x + y * cc.m_width + z * cc.m_width * cc.m_height;

    CubicalComplex::CCElmtType elmt = cc(x, y, z);

    if (elmt != 0) {
        if (cc.isFreeXNEG(x,y,z))
            borderIndex[idx] = 0;

        else if (cc.isFreeXPOS(x,y,z))
            borderIndex[idx] = 1;

        else if (cc.isFreeYNEG(x,y,z))
            borderIndex[idx] = 2;

        else if (cc.isFreeYPOS(x,y,z))
            borderIndex[idx] = 3;

        else if (cc.isFreeZNEG(x,y,z))
            borderIndex[idx] = 4;

        else if (cc.isFreeZPOS(x,y,z))
            borderIndex[idx] = 5;
    } else {
        borderIndex[idx] = 6;
    }
}

void CubicalComplex::cudaCalculateFirstBorder() {
    const dim3 blockSize(8, 8, 8);
    const dim3 gridSize(m_width / blockSize.x + (m_width % blockSize.x != 0),
                        m_height / blockSize.y + (m_height % blockSize.y != 0),
                        m_depth / blockSize.y + (m_depth % blockSize.y != 0));

    int* d_borderIndex;
    if(cudaMalloc((void**) &d_borderIndex, m_faces.size() * sizeof(int))) {
        std::cerr << "Oups" << std::endl;
        return;
    }

    CubicalComplex::CCElmtType* d_grid;
    if(cudaMalloc((void**) &d_grid, m_faces.size() * sizeof(CubicalComplex::CCElmtType))) {
        std::cerr << "Oups 2" << std::endl;
        return;
    }

    if(cudaMemcpy(d_grid, m_faces.data(), m_faces.size() * sizeof(CubicalComplex::CCElmtType), cudaMemcpyHostToDevice)) {
        std::cerr << "Oups 3" << std::endl;
        return;
    }

    //thrust::device_vector<CubicalComplex::CCElmtType> d_grid(m_faces);

    //thrust::device_vector<int> d_borderIndex(m_faces.size());

    CudaCubicalComplex cc;
    cc.m_faces = d_grid;
    cc.m_width = m_width;
    cc.m_height = m_height;
    cc.m_depth = m_depth;

    testForBorderKernel<<< gridSize, blockSize >>>(cc, d_borderIndex);
}
