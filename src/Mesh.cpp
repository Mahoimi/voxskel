#include "Mesh.h"

#include <cstddef>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

Mesh::MeshEntry::MeshEntry(): NumIndices(0)
{
    //Generate VB, IB and VAO
    glGenBuffers(1, &VB);
    glGenBuffers(1, &IB);
    glGenVertexArrays(1, &VAO);
}

Mesh::MeshEntry::MeshEntry(MeshEntry&& rvalue) {
    *this = std::move(rvalue);
}

Mesh::MeshEntry& Mesh::MeshEntry::operator =(MeshEntry&& rvalue) {
    VB = rvalue.VB;
    IB = rvalue.IB;
    VAO = rvalue.VAO;
    NumIndices = rvalue.NumIndices;

    rvalue.VB = 0;
    rvalue.IB = 0;
    rvalue.VAO = 0;
    rvalue.NumIndices = 0;

    return *this;
}

void Mesh::MeshEntry::init(const std::vector<Vertex>& Vertices,
                          const std::vector<unsigned int>& Indices)
{
    NumIndices = Indices.size();

    glBindBuffer(GL_ARRAY_BUFFER, VB);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * Vertices.size(), &Vertices[0], GL_STATIC_DRAW);
    
    glBindVertexArray(VAO);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, position));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, normal));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IB);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * NumIndices, &Indices[0], GL_STATIC_DRAW);
}

Mesh::MeshEntry::~MeshEntry()
{
    glDeleteBuffers(1, &VB);
    glDeleteBuffers(1, &IB);
    glDeleteVertexArrays(1, &VAO);
}

bool Mesh::load(const std::string& Filename)
{
    bool Ret = false;
    Assimp::Importer Importer;

    const aiScene* pScene = Importer.ReadFile(Filename.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

    if (pScene) {
        Ret = initFromScene(pScene, Filename);
    }
    else {
        printf("Error parsing '%s': '%s'\n", Filename.c_str(), Importer.GetErrorString());
    }

    return Ret;
}

bool Mesh::initFromScene(const aiScene* pScene, const std::string& Filename)
{
    //m_Entries.resize(pScene->mNumMeshes);

    // Initialize the meshes in the scene one by one
    for (unsigned int i = 0 ; i < pScene->mNumMeshes; i++) {
        const aiMesh* paiMesh = pScene->mMeshes[i];
        initMesh(i, paiMesh);
    }

    glm::vec3 size = m_BBoxMax - m_BBoxMin;
    m_AABCLength = std::max(std::max(size.x, size.y), size.z);
    m_center = 0.5f * (m_BBoxMax + m_BBoxMin);

    return true;
}

void Mesh::initMesh(unsigned int Index, const aiMesh* paiMesh)
{
    std::vector<Vertex> Vertices;
    std::vector<unsigned int> Indices;

    const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    //Values used to determine the bounding box of the mesh
    if(m_Entries.empty()) {
        m_BBoxMin = m_BBoxMax = glm::vec3(
                    paiMesh->mVertices[0].x,
                    paiMesh->mVertices[0].y,
                    paiMesh->mVertices[0].z);
    }

/*
    GLfloat min_x, max_x, min_y, max_y, min_z, max_z;
    min_x = max_x = paiMesh->mVertices[0].x;
    min_y = max_y = paiMesh->mVertices[0].y;
    min_z = max_z = paiMesh->mVertices[0].z;
*/
    for (unsigned int i = 0 ; i < paiMesh->mNumVertices ; i++) {
        const aiVector3D* pPos      = &(paiMesh->mVertices[i]);
        const aiVector3D* pNormal   = &(paiMesh->mNormals[i]);
        const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

        Vertex v;
        v.position=glm::vec3(pPos->x, pPos->y, pPos->z);
        v.texCoords=glm::vec2(pTexCoord->x, pTexCoord->y);
        v.normal=glm::vec3(pNormal->x, pNormal->y, pNormal->z);

        //Bounding box algorithm

        /*
        if (pPos->x < min_x) min_x = pPos->x;
        if (pPos->x > max_x) max_x = pPos->x;
        if (pPos->y < min_y) min_y = pPos->y;
        if (pPos->y > max_y) max_y = pPos->y;
        if (pPos->z < min_z) min_z = pPos->z;
        if (pPos->z > max_z) max_z = pPos->z;
*/
        m_BBoxMin = glm::min(m_BBoxMin, v.position);
        m_BBoxMax = glm::max(m_BBoxMax, v.position);

        Vertices.push_back(v);
    }

    /*
    glm::vec3 size = glm::vec3(max_x - min_x, max_y - min_y, max_z - min_z);
    m_center = glm::vec3((min_x + max_x) * 0.5f, (min_y + max_y) * 0.5f, (min_z + max_z) * 0.5f);
    m_AABCLength = std::max(std::max(size.x, size.y), size.z);
*/

    for (unsigned int i = 0 ; i < paiMesh->mNumFaces ; i++) {
        const aiFace& Face = paiMesh->mFaces[i];
        assert(Face.mNumIndices == 3);
        Indices.push_back(Face.mIndices[0]);
        Indices.push_back(Face.mIndices[1]);
        Indices.push_back(Face.mIndices[2]);
    }

    //m_Entries[Index].init(Vertices, Indices);

    m_Entries.emplace_back(MeshEntry());
    m_Entries.back().init(Vertices, Indices);
}

void Mesh::render() const
{
    for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {
        glBindVertexArray(m_Entries[i].VAO);
        glDrawElements(GL_TRIANGLES, m_Entries[i].NumIndices, GL_UNSIGNED_INT, 0);
    }
}
