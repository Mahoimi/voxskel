#include "CubicalComplex.h"
#include <iostream>
#include <stack>

void CubicalComplex::calculateFirstBorder() {
    //cudaCalculateFirstBorder();

    for(std::size_t z = 0; z < m_depth; ++z){
        for(std::size_t y = 0; y < m_height; ++y){
            for(std::size_t x = 0; x < m_width; ++x){
                testForBorder(x,y,z);
            }
        }
    }
}

void CubicalComplex::updateBorder() {
    std::vector<glm::ivec3> updatedBorder;

    updateBorderXNEG(updatedBorder);
    updateBorderXPOS(updatedBorder);
    updateBorderYNEG(updatedBorder);
    updateBorderYPOS(updatedBorder);
    updateBorderZNEG(updatedBorder);
    updateBorderZPOS(updatedBorder);

    clearBorder();
    clearBoolBorder(updatedBorder);

    calculateUpdatedBorder(updatedBorder);
}

void CubicalComplex::calculateUpdatedBorder(const std::vector<glm::ivec3>& updatedBorder){
    for (auto i = 0; i < updatedBorder.size(); i++){
        glm::ivec3 p = updatedBorder[i];
        testForBorder(p.x, p.y, p.z);
    }
}

void CubicalComplex::updateBorderXNEG(std::vector<glm::ivec3>& updatedBorder) {
   for (auto i = 0; i < m_borders[0].size(); i++){
        glm::ivec3 p = m_borders[0][i];

        if (!isInBorder(p.x, p.y, p.z)){
            updatedBorder.emplace_back(p);
            setInBorder(p.x, p.y, p.z);
        }

        // Testing neighbourhood
        if (p.x > 0 && !isInBorder(p.x - 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x - 1, p.y, p.z));
            setInBorder(p.x - 1, p.y, p.z);
        }

        if (p.y > 0 && !isInBorder(p.x, p.y - 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y - 1, p.z));
            setInBorder(p.x, p.y - 1, p.z);
        }

        if (p.y < m_height - 1 && !isInBorder(p.x, p.y + 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y + 1, p.z));
            setInBorder(p.x, p.y + 1, p.z);
        }

        if (p.z > 0 && !isInBorder(p.x, p.y, p.z - 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z - 1));
            setInBorder(p.x, p.y, p.z - 1);
        }

        if (p.z < m_depth - 1 && !isInBorder(p.x, p.y, p.z + 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z + 1));
            setInBorder(p.x, p.y, p.z + 1);
        }
    }
}

void CubicalComplex::updateBorderXPOS(std::vector<glm::ivec3>& updatedBorder) {
   for (auto i = 0; i < m_borders[1].size(); i++){
        glm::ivec3 p = m_borders[1][i];

        if (!isInBorder(p.x, p.y, p.z)){
            updatedBorder.emplace_back(p);
            setInBorder(p.x, p.y, p.z);
        }

        // Testing neighbourhood
        if (p.x < m_width - 1 && !isInBorder(p.x + 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x + 1, p.y, p.z));
            setInBorder(p.x + 1, p.y, p.z);
        }

        if (p.y > 0 && isFreeXPOS(p.x, p.y - 1, p.z) && !isInBorder(p.x, p.y - 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y - 1, p.z));
            setInBorder(p.x, p.y - 1, p.z);
        }

        if (p.y < m_height - 1 && !isInBorder(p.x, p.y + 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y + 1, p.z));
            setInBorder(p.x, p.y + 1, p.z);
        }

        if (p.z > 0 && !isInBorder(p.x, p.y, p.z - 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z - 1));
            setInBorder(p.x, p.y, p.z - 1);
        }

        if (p.z < m_depth - 1 && !isInBorder(p.x, p.y, p.z + 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z + 1));
            setInBorder(p.x, p.y, p.z + 1);
        }

    }
}

void CubicalComplex::updateBorderYNEG(std::vector<glm::ivec3>& updatedBorder) {
    for (auto i = 0; i < m_borders[2].size(); i++){
        glm::ivec3 p = m_borders[2][i];

        if (!isInBorder(p.x, p.y, p.z)){
            updatedBorder.emplace_back(p);
            setInBorder(p.x, p.y, p.z);
        }

        // Testing neighbourhood
        if (p.x > 0 && !isInBorder(p.x - 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x - 1, p.y, p.z));
            setInBorder(p.x - 1, p.y, p.z);
        }

        if (p.x < m_width - 1 && !isInBorder(p.x + 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x + 1, p.y, p.z));
            setInBorder(p.x + 1, p.y, p.z);
        }

        if (p.y > 0 && !isInBorder(p.x, p.y - 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y - 1, p.z));
            setInBorder(p.x, p.y - 1, p.z);
        }

        if (p.z > 0 && !isInBorder(p.x, p.y, p.z - 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z - 1));
            setInBorder(p.x, p.y, p.z - 1);
        }

        if (p.z < m_depth - 1 && !isInBorder(p.x, p.y, p.z + 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z + 1));
            setInBorder(p.x, p.y, p.z + 1);
        }
    }

}

void CubicalComplex::updateBorderYPOS(std::vector<glm::ivec3>& updatedBorder) {
   for (auto i = 0; i < m_borders[3].size(); i++){
        glm::ivec3 p = m_borders[3][i];

        if (!isInBorder(p.x, p.y, p.z)){
            updatedBorder.emplace_back(p);
            setInBorder(p.x, p.y, p.z);
        }

        // Testing neighbourhood
        if (p.x > 0 && !isInBorder(p.x - 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x - 1, p.y, p.z));
            setInBorder(p.x - 1, p.y, p.z);
        }

        if (p.x < m_width - 1 && !isInBorder(p.x + 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x + 1, p.y, p.z));
            setInBorder(p.x + 1, p.y, p.z);
        }

        if (p.y < m_height - 1 && !isInBorder(p.x, p.y + 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y + 1, p.z));
            setInBorder(p.x, p.y + 1, p.z);
        }

        if (p.z > 0 && !isInBorder(p.x, p.y, p.z - 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z - 1));
            setInBorder(p.x, p.y, p.z - 1);
        }

        if (p.z < m_depth - 1 && !isInBorder(p.x, p.y, p.z + 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z + 1));
            setInBorder(p.x, p.y, p.z + 1);
        }

    }
}

void CubicalComplex::updateBorderZNEG(std::vector<glm::ivec3>& updatedBorder) {
    for (auto i = 0; i < m_borders[4].size(); i++){
        glm::ivec3 p = m_borders[4][i];

        if (!isInBorder(p.x, p.y, p.z)){
            updatedBorder.emplace_back(p);
            setInBorder(p.x, p.y, p.z);
        }

        // Testing neighbourhood
        if (p.x > 0 && !isInBorder(p.x - 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x - 1, p.y, p.z));
            setInBorder(p.x - 1, p.y, p.z);
        }

        if (p.x < m_width - 1 && !isInBorder(p.x + 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x + 1, p.y, p.z));
            setInBorder(p.x + 1, p.y, p.z);
        }

        if (p.y > 0 && !isInBorder(p.x, p.y - 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y - 1, p.z));
            setInBorder(p.x, p.y - 1, p.z);
        }

        if (p.y < m_height - 1 && !isInBorder(p.x, p.y + 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y + 1, p.z));
            setInBorder(p.x, p.y + 1, p.z);
        }

        if (p.z > 0 && !isInBorder(p.x, p.y, p.z - 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z - 1));
            setInBorder(p.x, p.y, p.z - 1);
        }
    }
}

void CubicalComplex::updateBorderZPOS(std::vector<glm::ivec3>& updatedBorder) {
   for (auto i = 0; i < m_borders[5].size(); i++){
        glm::ivec3 p = m_borders[5][i];

        if (!isInBorder(p.x, p.y, p.z)){
            updatedBorder.emplace_back(p);
            setInBorder(p.x,p.y,p.z);
        }

        // Testing neighbourhood
        if (p.x > 0 && !isInBorder(p.x - 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x - 1, p.y, p.z));
            setInBorder(p.x - 1, p.y, p.z);
        }

        if (p.x < m_width - 1 && !isInBorder(p.x + 1, p.y, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x + 1, p.y, p.z));
            setInBorder(p.x + 1, p.y, p.z);
        }

        if (p.y > 0 && !isInBorder(p.x, p.y - 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y - 1, p.z));
            setInBorder(p.x, p.y - 1, p.z);
        }

        if (p.y < m_height - 1 && !isInBorder(p.x, p.y + 1, p.z)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y + 1, p.z));
            setInBorder(p.x, p.y + 1, p.z);
        }

        if (p.z < m_depth - 1 && !isInBorder(p.x, p.y, p.z + 1)){
            updatedBorder.emplace_back(glm::ivec3(p.x, p.y, p.z + 1));
            setInBorder(p.x, p.y, p.z + 1);
        }
    }
}

void CubicalComplex::clearBoolBorder(const std::vector<glm::ivec3>& updatedBorder) {
    for (auto i = 0; i < updatedBorder.size(); ++i){
        glm::ivec3 p = updatedBorder[i];
        setInBorder(p.x, p.y, p.z, false);
    }
}

void CubicalComplex::clearBorder() {
    for (int i = 0; i < 6; i++){
        m_borders[i].clear();
    }
}

void CubicalComplex::testForBorder(int x, int y, int z) {
    if ((*this)(x,y,z).exists()){
        if (isFreeXNEG(x,y,z))
            m_borders[0].emplace_back(glm::ivec3(x,y,z)); //dir = 0 or = 0

        else if (isFreeXPOS(x,y,z))
            m_borders[1].emplace_back(glm::ivec3(x,y,z)); //dir = 0 or = 1

        else if (isFreeYNEG(x,y,z))
            m_borders[2].emplace_back(glm::ivec3(x,y,z)); //dir = 1 or = 0

        else if (isFreeYPOS(x,y,z))
            m_borders[3].emplace_back(glm::ivec3(x,y,z)); //dir = 1 or = 1

        else if (isFreeZNEG(x,y,z))
            m_borders[4].emplace_back(glm::ivec3(x,y,z)); //dir = 2 or = 0

        else if (isFreeZPOS(x,y,z))
            m_borders[5].emplace_back(glm::ivec3(x,y,z)); //dir = 2 or = 1

    }
}

bool CubicalComplex::isFreeXNEG(int x, int y, int z) const{
    return isFreeXNEG1D(x, y, z) || isFreeXNEG2D(x, y, z) || isFreeXNEG3D(x, y, z);
}

bool CubicalComplex::isFreeXNEG3D(int x, int y, int z) const{
    return x > 0 && (*this)(x - 1, y, z).has(CUBE) && !(*this)(x, y, z).has(CUBE);
}

bool CubicalComplex::isFreeXNEG2D(int x, int y, int z) const{
    return isFreeXNEG_XYFACE(x,y,z) || isFreeXNEG_XZFACE(x,y,z);
}

bool CubicalComplex::isFreeXNEG_XYFACE(int x, int y, int z) const{
    return x > 0 && !(*this)(x, y, z).has(YZFACE) &&
          (*this)(x - 1, y, z).has(XYFACE) && !(*this)(x, y, z).has(XYFACE) && (z == 0 || !(*this)(x, y, z - 1).has(YZFACE));
}

bool CubicalComplex::isFreeXNEG_XZFACE(int x, int y, int z) const{
    return x > 0 && !(*this)(x, y, z).has(YZFACE) &&
          (*this)(x - 1, y, z).has(XZFACE) && !(*this)(x, y, z).has(XZFACE) && (y == 0 || !(*this)(x, y - 1, z).has(YZFACE));
}

bool CubicalComplex::isFreeXNEG1D(int x, int y, int z) const{
    return x > 0 && !(*this)(x,y,z).has(YEDGE) && !(*this)(x,y,z).has(ZEDGE) && !(*this)(x,y,z).has(XEDGE) && (*this)(x - 1,y,z).has(XEDGE) &&
            (z == 0 || !(*this)(x, y, z - 1).has(ZEDGE)) && (y == 0 || !(*this)(x, y - 1, z).has(YEDGE));
}

bool CubicalComplex::isFreeXPOS(int x, int y, int z) const{
    return isFreeXPOS1D(x, y, z) || isFreeXPOS2D(x, y, z) || isFreeXPOS3D(x, y, z);
}

bool CubicalComplex::isFreeXPOS3D(int x, int y, int z) const{
    return x < m_width - 1 && (*this)(x,y,z).has(CUBE) && (x == 0  || !(*this)(x - 1, y, z).has(CUBE));
}

bool CubicalComplex::isFreeXPOS2D(int x, int y, int z) const{
    return isFreeXPOS_XYFACE(x, y, z) || isFreeXPOS_XZFACE(x, y, z);
}

bool CubicalComplex::isFreeXPOS_XYFACE(int x, int y, int z) const{
    return x < m_width - 1 && !(*this)(x, y, z).has(YZFACE) &&
          (*this)(x, y, z).has(XYFACE) && (x == 0 || !(*this)(x - 1, y, z).has(XYFACE)) && (z == 0 || !(*this)(x, y, z - 1).has(YZFACE));
}

bool CubicalComplex::isFreeXPOS_XZFACE(int x, int y, int z) const{
    return x < m_width - 1 && !(*this)(x, y, z).has(YZFACE) &&
          (*this)(x, y, z).has(XZFACE) && (x == 0 || !(*this)(x - 1, y, z).has(XZFACE)) && (y == 0 || !(*this)(x, y - 1, z).has(YZFACE));
}

bool CubicalComplex::isFreeXPOS1D(int x, int y, int z) const{
    return x < m_width - 1 && !(*this)(x,y,z).has(YEDGE) && !(*this)(x,y,z).has(ZEDGE) && (*this)(x,y,z).has(XEDGE) &&
            (z == 0 || !(*this)(x, y, z - 1).has(ZEDGE)) && (y == 0 || !(*this)(x, y - 1, z).has(YEDGE)) && (x == 0 || !(*this)(x - 1, y, z).has(XEDGE));
}

bool CubicalComplex::isFreeYNEG(int x, int y, int z) const{
   return isFreeYNEG1D(x, y, z) || isFreeYNEG2D(x, y, z) || isFreeYNEG3D(x, y, z);
}

bool CubicalComplex::isFreeYNEG3D(int x, int y, int z) const{
    return y > 0 && (*this)(x , y - 1, z).has(CUBE) && !(*this)(x, y, z).has(CUBE);
}

bool CubicalComplex::isFreeYNEG2D(int x, int y, int z) const{
    return isFreeYNEG_XYFACE(x,y,z) || isFreeYNEG_YZFACE(x,y,z);
}

bool CubicalComplex::isFreeYNEG_XYFACE(int x, int y, int z) const{
    return y > 0 && !(*this)(x, y, z).has(XZFACE) &&
          (*this)(x, y - 1, z).has(XYFACE) && !(*this)(x, y, z).has(XYFACE) && (z == 0 || !(*this)(x, y, z - 1).has(XZFACE));
}

bool CubicalComplex::isFreeYNEG_YZFACE(int x, int y, int z) const{
    return y > 0 && !(*this)(x, y, z).has(XZFACE) &&
          (*this)(x, y - 1, z).has(YZFACE) && !(*this)(x, y, z).has(YZFACE) && (x == 0 || !(*this)(x - 1, y, z).has(XZFACE));
}

bool CubicalComplex::isFreeYNEG1D(int x, int y, int z) const{
    return y > 0 && !(*this)(x, y, z).has(XEDGE) && !(*this)(x, y, z).has(YEDGE) && !(*this)(x, y, z).has(ZEDGE) && (*this)(x, y - 1, z).has(YEDGE) &&
            (z == 0 || !(*this)(x, y, z - 1).has(ZEDGE)) && (x == 0 || !(*this)(x - 1, y, z).has(XEDGE));
}

bool CubicalComplex::isFreeYPOS(int x, int y, int z) const{
    return isFreeYPOS3D(x,y,z) || isFreeYPOS2D(x,y,z) || isFreeYPOS1D(x,y,z);
}

bool CubicalComplex::isFreeYPOS3D(int x, int y, int z) const{
    return y < m_height - 1 && (*this)(x,y,z).has(CUBE) && (y == 0  || !(*this)(x, y - 1, z).has(CUBE));
}

bool CubicalComplex::isFreeYPOS2D(int x, int y, int z) const{
    return isFreeYPOS_XYFACE(x,y,z) || isFreeYPOS_YZFACE(x,y,z);
}

bool CubicalComplex::isFreeYPOS_XYFACE(int x, int y, int z) const{
    return y < m_height - 1 && !(*this)(x, y, z).has(XZFACE) &&
          (*this)(x, y, z).has(XYFACE) && (y == 0 || !(*this)(x, y - 1, z).has(XYFACE)) && (z == 0 || !(*this)(x, y, z - 1).has(XZFACE));
}

bool CubicalComplex::isFreeYPOS_YZFACE(int x, int y, int z) const{
    return y < m_height - 1 && !(*this)(x, y, z).has(XZFACE) &&
          (*this)(x, y, z).has(YZFACE) && (y == 0 || !(*this)(x, y - 1, z).has(YZFACE)) && (x == 0 || !(*this)(x - 1, y, z).has(XZFACE));
}

bool CubicalComplex::isFreeYPOS1D(int x, int y, int z) const{
    return y < m_height - 1 && !(*this)(x,y,z).has(XEDGE) && !(*this)(x,y,z).has(ZEDGE) && (*this)(x,y,z).has(YEDGE) &&
            (z == 0 || !(*this)(x, y, z - 1).has(ZEDGE)) && (y == 0 || !(*this)(x, y - 1, z).has(YEDGE)) && (x == 0 || !(*this)(x - 1, y, z).has(XEDGE));
}

bool CubicalComplex::isFreeZNEG(int x, int y, int z) const{
    return isFreeZNEG3D(x,y,z) || isFreeZNEG2D(x,y,z) || isFreeZNEG1D(x,y,z);
}

bool CubicalComplex::isFreeZNEG3D(int x, int y, int z) const{
     return z > 0 && (*this)(x , y, z - 1).has(CUBE) && !(*this)(x, y, z).has(CUBE);
}

bool CubicalComplex::isFreeZNEG2D(int x, int y, int z) const{
    return isFreeZNEG_XZFACE(x,y,z) || isFreeZNEG_YZFACE(x,y,z);
}

bool CubicalComplex::isFreeZNEG_XZFACE(int x, int y, int z) const{
    return z > 0 && !(*this)(x, y, z).has(XYFACE) &&
          (*this)(x, y, z - 1).has(XZFACE) && !(*this)(x, y, z).has(XZFACE) && (y == 0 || !(*this)(x, y - 1, z).has(XYFACE));
}

bool CubicalComplex::isFreeZNEG_YZFACE(int x, int y, int z) const{
    return z > 0 && !(*this)(x, y, z).has(XYFACE) &&
          (*this)(x, y, z - 1).has(YZFACE) && !(*this)(x, y, z).has(YZFACE) && (x == 0 || !(*this)(x - 1, y, z).has(XYFACE));
}

bool CubicalComplex::isFreeZNEG1D(int x, int y, int z) const{
    return z > 0 && !(*this)(x, y, z).has(XEDGE) && !(*this)(x, y, z).has(YEDGE) && !(*this)(x, y, z).has(ZEDGE) && (*this)(x, y, z - 1).has(ZEDGE) &&
            (y == 0 || !(*this)(x, y - 1, z).has(YEDGE)) && (x == 0 || !(*this)(x - 1, y, z).has(XEDGE));
}

bool CubicalComplex::isFreeZPOS(int x, int y, int z) const{
    return isFreeZPOS3D(x,y,z) || isFreeZPOS2D(x,y,z) || isFreeZPOS1D(x,y,z);
}

bool CubicalComplex::isFreeZPOS3D(int x, int y, int z) const{
    return z < m_height - 1 && (*this)(x,y,z).has(CUBE) && (z == 0  || !(*this)(x, y, z - 1).has(CUBE));
}

bool CubicalComplex::isFreeZPOS2D(int x, int y, int z) const{
    return isFreeZPOS_XZFACE(x,y,z) || isFreeZPOS_YZFACE(x,y,z);
}

bool CubicalComplex::isFreeZPOS_XZFACE(int x, int y, int z) const{
    return z < m_height - 1 && !(*this)(x, y, z).has(XYFACE) &&
          (*this)(x, y, z).has(XZFACE) && (z == 0 || !(*this)(x, y, z - 1).has(XZFACE)) && (y == 0 || !(*this)(x, y - 1, z ).has(XYFACE));
}

bool CubicalComplex::isFreeZPOS_YZFACE(int x, int y, int z) const{
    return z < m_height - 1 && !(*this)(x, y, z).has(XYFACE) &&
          (*this)(x, y, z).has(YZFACE) && (z == 0 || !(*this)(x, y, z - 1).has(YZFACE)) && (x == 0 || !(*this)(x - 1, y, z).has(XYFACE));
}

bool CubicalComplex::isFreeZPOS1D(int x, int y, int z) const{
    return z < m_height - 1 && !(*this)(x,y,z).has(XEDGE) && !(*this)(x,y,z).has(YEDGE) && (*this)(x,y,z).has(ZEDGE) &&
            (z == 0 || !(*this)(x, y, z - 1).has(ZEDGE)) && (y == 0 || !(*this)(x, y - 1, z).has(YEDGE)) && (x == 0 || !(*this)(x - 1, y, z).has(XEDGE));
}

void CubicalComplex::parDirCollapse (int iter) {
    while(iter > 0) {
        collapseFreePairsXNeg3D(m_borders[0]);
        collapseFreePairsXNeg2D(m_borders[0]);
        collapseFreePairsXNeg1D(m_borders[0]);
        collapseFreePairsXPos3D(m_borders[1]);
        collapseFreePairsXPos2D(m_borders[1]);
        collapseFreePairsXPos1D(m_borders[1]);

        collapseFreePairsYNeg3D(m_borders[2]);
        collapseFreePairsYNeg2D(m_borders[2]);
        collapseFreePairsYNeg1D(m_borders[2]);
        collapseFreePairsYPos3D(m_borders[3]);
        collapseFreePairsYPos2D(m_borders[3]);
        collapseFreePairsYPos1D(m_borders[3]);

        collapseFreePairsZNeg3D(m_borders[4]);
        collapseFreePairsZNeg2D(m_borders[4]);
        collapseFreePairsZNeg1D(m_borders[4]);
        collapseFreePairsZPos3D(m_borders[5]);
        collapseFreePairsZPos2D(m_borders[5]);
        collapseFreePairsZPos1D(m_borders[5]);

        iter--;
        updateBorder();  
    }
}

void CubicalComplex::parDirCollapse () {
    while(stillFreeFaces()){
        collapseFreePairsXNeg3D(m_borders[0]);
        collapseFreePairsXNeg2D(m_borders[0]);
        collapseFreePairsXNeg1D(m_borders[0]);
        collapseFreePairsXPos3D(m_borders[1]);
        collapseFreePairsXPos2D(m_borders[1]);
        collapseFreePairsXPos1D(m_borders[1]);

        collapseFreePairsYNeg3D(m_borders[2]);
        collapseFreePairsYNeg2D(m_borders[2]);
        collapseFreePairsYNeg1D(m_borders[2]);
        collapseFreePairsYPos3D(m_borders[3]);
        collapseFreePairsYPos2D(m_borders[3]);
        collapseFreePairsYPos1D(m_borders[3]);

        collapseFreePairsZNeg3D(m_borders[4]);
        collapseFreePairsZNeg2D(m_borders[4]);
        collapseFreePairsZNeg1D(m_borders[4]);
        collapseFreePairsZPos3D(m_borders[5]);
        collapseFreePairsZPos2D(m_borders[5]);
        collapseFreePairsZPos1D(m_borders[5]);

        updateBorder();
    }
}

void CubicalComplex::collapseFreePairsXPos3D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeXPOS3D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(CUBE | YZFACE);
        }
    }
}

void CubicalComplex::collapseFreePairsXPos2D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeXPOS2D(voxel.x, voxel.y, voxel.z)){
            if(isFreeXPOS_XZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(XZFACE | ZEDGE);
            }

            if(isFreeXPOS_XYFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(XYFACE | YEDGE);
            }
        }
    }
}

void CubicalComplex::collapseFreePairsXPos1D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeXPOS1D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(XEDGE | POINT);
        }
    }
}

void CubicalComplex::collapseFreePairsXNeg3D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeXNEG3D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(YZFACE);
            (*this)(voxel.x - 1, voxel.y, voxel.z).remove(CUBE);
        }
    }
}

void CubicalComplex::collapseFreePairsXNeg2D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeXNEG2D(voxel.x, voxel.y, voxel.z)){

            if(isFreeXNEG_XZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(ZEDGE);
                (*this)(voxel.x - 1, voxel.y, voxel.z).remove(XZFACE);
            }

            if(isFreeXNEG_XYFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(YEDGE);
                (*this)(voxel.x - 1, voxel.y, voxel.z).remove(XYFACE);
            }
        }
    }
}

void CubicalComplex::collapseFreePairsXNeg1D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeXNEG1D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(POINT);
            (*this)(voxel.x - 1, voxel.y, voxel.z).remove(XEDGE);
        }
    }
}

void CubicalComplex::collapseFreePairsYPos3D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeYPOS3D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(CUBE | XZFACE);
        }
    }
}

void CubicalComplex::collapseFreePairsYPos2D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeYPOS2D(voxel.x, voxel.y, voxel.z)){
            if(isFreeYPOS_YZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(YZFACE | ZEDGE);
            }

            if(isFreeYPOS_XYFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(XYFACE | XEDGE);
            }
        }
    }
}

void CubicalComplex::collapseFreePairsYPos1D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeYPOS1D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(YEDGE | POINT);
        }
    }
}

void CubicalComplex::collapseFreePairsYNeg3D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeYNEG3D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(XZFACE);
            (*this)(voxel.x, voxel.y - 1, voxel.z).remove(CUBE);
        }
    }
}

void CubicalComplex::collapseFreePairsYNeg2D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeYNEG2D(voxel.x, voxel.y, voxel.z)){
            if(isFreeYNEG_YZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(ZEDGE);
                (*this)(voxel.x, voxel.y - 1, voxel.z).remove(YZFACE);
            }

            if(isFreeYNEG_XYFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(XEDGE);
                (*this)(voxel.x, voxel.y - 1, voxel.z).remove(XYFACE);
            }
        }
    }
}

void CubicalComplex::collapseFreePairsYNeg1D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeYNEG1D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(POINT);
            (*this)(voxel.x, voxel.y - 1, voxel.z).remove(YEDGE);
        }
    }
}

void CubicalComplex::collapseFreePairsZPos3D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeZPOS3D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(CUBE | XYFACE);
        }
    }
}

void CubicalComplex::collapseFreePairsZPos2D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeZPOS2D(voxel.x, voxel.y, voxel.z)){
            if(isFreeZPOS_YZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(YZFACE | YEDGE);
            }

            if(isFreeZPOS_XZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(XZFACE | XEDGE);
            }
        }
    }
}

void CubicalComplex::collapseFreePairsZPos1D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeZPOS1D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(ZEDGE | POINT);
        }
    }
}

void CubicalComplex::collapseFreePairsZNeg3D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeZNEG3D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(XYFACE);
            (*this)(voxel.x, voxel.y, voxel.z - 1).remove(CUBE);
        }
    }
}

void CubicalComplex::collapseFreePairsZNeg2D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeZNEG2D(voxel.x, voxel.y, voxel.z)){
            if(isFreeZNEG_YZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(YEDGE);
                (*this)(voxel.x, voxel.y, voxel.z - 1).remove(YZFACE);
            }

            if(isFreeZNEG_XZFACE(voxel.x, voxel.y, voxel.z)) {
                (*this)(voxel.x, voxel.y, voxel.z).remove(XEDGE);
                (*this)(voxel.x, voxel.y, voxel.z - 1).remove(XZFACE);
            }
        }
    }
}

void CubicalComplex::collapseFreePairsZNeg1D(const std::vector<glm::ivec3>& border) {
    for(const auto& voxel: border) {
        if(isFreeZNEG1D(voxel.x, voxel.y, voxel.z)) {
            (*this)(voxel.x, voxel.y, voxel.z).remove(POINT);
            (*this)(voxel.x, voxel.y, voxel.z - 1).remove(ZEDGE);
        }
    }
}

uint32_t CubicalComplex::recursiveDepthFirstTraversal(int x, int y, int z, std::vector<bool>& visited) const {
    std::stack<glm::ivec3> stack;

    auto idx = index(x, y, z);
    glm::ivec3 parent = glm::ivec3(x, y, z);

    if(!isInGrid(x, y, z) || visited[idx] || !m_faces[idx].exists()) {
        return 0;
    }

    visited[idx] = true;
    stack.push(parent);

    auto f = [&](int x, int y, int z, facet_data element) {
        auto voxel = glm::ivec3(x, y, z);
        if(isInGrid(voxel.x, voxel.y, voxel.z)) {
            auto idx = index(voxel.x, voxel.y, voxel.z);
            if(!visited[idx] && m_faces[idx].has(element)) {
                visited[idx] = true;
                stack.push(voxel);
            }
        }
    };

    while(!stack.empty()) {
        parent = stack.top();
        stack.pop();
        x = parent.x;
        y = parent.y;
        z = parent.z;
        idx = index(x, y, z);

        if(m_faces[idx].has(POINT)) {
            f(x - 1, y, z, XEDGE);
            f(x, y, z - 1, ZEDGE);
            f(x, y - 1, z, YEDGE);
            f(x - 1, y, z - 1, XZFACE);
            f(x - 1, y - 1, z, XYFACE);
            f(x, y - 1, z - 1, YZFACE);
            f(x - 1, y - 1, z - 1, CUBE);
        }

        if(m_faces[idx].has(XEDGE)) {
            f(x + 1, y, z, POINT);
            f(x + 1, y, z - 1, ZEDGE);
            f(x + 1, y - 1, z, YEDGE);
            f(x + 1, y - 1, z - 1, YZFACE);
        }

        if(m_faces[idx].has(YEDGE)) {
            f(x, y + 1, z, POINT);
            f(x - 1, y + 1, z, XEDGE);
            f(x, y + 1, z - 1, ZEDGE);
            f(x - 1, y + 1, z - 1, XZFACE);
        }

        if(m_faces[idx].has(ZEDGE)) {
            f(x, y, z + 1, POINT);
            f(x - 1, y, z + 1, XEDGE);
            f(x, y - 1, z + 1, YEDGE);
            f(x - 1, y - 1, z + 1, XYFACE);
        }

        if(m_faces[idx].has(XYFACE)) {
            f(x + 1, y + 1, z, POINT);
            f(x + 1, y + 1, z - 1, ZEDGE);
        }

        if(m_faces[idx].has(XZFACE)) {
            f(x + 1, y, z + 1, POINT);
            f(x + 1, y - 1, z + 1, YEDGE);
        }

        if(m_faces[idx].has(YZFACE)) {
            f(x, y + 1, z + 1, POINT);
            f(x - 1, y + 1, z + 1, XEDGE);
        }

        if(m_faces[idx].has(CUBE)) {
            f(x + 1, y + 1, z + 1, POINT);
        }
    }

    return 1;
}

uint32_t CubicalComplex::computeConnexityNumber() const {
    std::vector<bool> visited(m_faces.size(), false);

    uint32_t connexityNumber = 0;

    for(auto z = 0u; z < m_depth; ++z)  {
        for(auto y = 0u; y < m_height; ++y) {
            for(auto x = 0u; x < m_width; ++x) {
                connexityNumber += recursiveDepthFirstTraversal(x, y, z, visited);
            }
        }
    }

    return connexityNumber;
}

int CubicalComplex::computeEulerCharacteristic() const {
    int ki = 0;

    for(auto z = 0u; z < m_depth; ++z)  {
        for(auto y = 0u; y < m_height; ++y) {
            for(auto x = 0u; x < m_width; ++x) {
                auto idx = index(x, y, z);
                ki += (int(m_faces[idx].has(POINT))
                       - (int(m_faces[idx].has(XEDGE)) + int(m_faces[idx].has(YEDGE)) + int(m_faces[idx].has(ZEDGE)))
                       + (int(m_faces[idx].has(XYFACE)) + int(m_faces[idx].has(XZFACE)) + int(m_faces[idx].has(YZFACE)))
                       - int(m_faces[idx].has(CUBE)));
            }
        }
    }

    return ki;
}
