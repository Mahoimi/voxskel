#include "GLCubicalComplex.h"
#include <iostream>

void GLCubicalComplex::init(const CubicalComplex &cubicalComplex) {
    int size_x = cubicalComplex.width();
    int size_y = cubicalComplex.height();
    int size_z = cubicalComplex.depth();

    std::vector<CCVertex> CCVertices;

    glBindTexture(GL_TEXTURE_3D, m_CCTextureObject);

    std::vector<int32_t> tmp(size_x * size_y * size_z);
    std::cerr << tmp.size() << std::endl;
    for(int i = 0; i < tmp.size(); ++i) {
        tmp[i] = cubicalComplex.getFaces()[i].m_data;

        /*
        if(cubicalComplex.getFaces()[i].exists()) {
            tmp[i] = ALL;
        }*/
    }

    /*
    for(int z = 0; z < size_z; ++z) {
        for(int y = 0; y < size_y; ++y) {
            for(int x = 0; x < size_x * 0.5; ++x) {
                int i = x + y * size_x + z * size_x * size_y;
                tmp[i] = ALL;
            }
        }
    }*/

    glTexImage3D(GL_TEXTURE_3D, 0, GL_R8I, size_x, size_y, size_z, 0,
                 GL_RED_INTEGER, GL_INT, tmp.data());

    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_3D, 0);

    if(!m_nVertexCount) {
        for(int z = 0; z < size_z; ++z) {
            for(int y = 0; y < size_y; ++y) {
                for(int x = 0; x < size_x; ++x) {
                    glm::ivec3 position(x, y, z);
                    CCVertices.emplace_back(CCVertex(position));
                }
            }
        }

        m_nVertexCount = CCVertices.size();

        glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(CCVertex) * CCVertices.size(), CCVertices.data(), GL_STATIC_DRAW);

        glBindVertexArray(m_VAO);

        glEnableVertexAttribArray(0);

        glVertexAttribIPointer(0, 3, GL_INT, sizeof(CCVertex), (const GLvoid*) 0);
    }
}
