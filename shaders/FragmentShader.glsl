#version 330 core

in vec3 normal_view;
in vec3 position_view;

out vec3 color;
 
void main() {
    color = vec3(1) * abs(dot(normalize(normal_view), normalize(-position_view)));
}
