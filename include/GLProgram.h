#ifndef _GLPROGRAM_H_
#define _GLPROGRAM_H_

#include <GL/glew.h>
#include <vector>

class GLProgram {
	private:
        GLuint m_programID;
	public:
        GLProgram();

        GLuint load(const char* vertexShaderFile, const char* fragmentShaderFile, const char* geometryShaderFile = nullptr);

        GLuint getProgramID() const {
            return m_programID;
        }

        void use() const {
            glUseProgram(m_programID);
        }

        GLint getUniformLocation(const char* uniform) const {
            return glGetUniformLocation(m_programID, uniform);
        }
};

#endif
