#ifndef _VOXELIZER_H_
#define _VOXELIZER_H_

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include "VoxelGrid.hpp"
#include "CubicalComplex.h"
#include "Mesh.h"
#include "GLProgram.h"
#include "GLVoxelFramebuffer.h"
#include "VoxelSpace.hpp"

class GLVoxelizer {
public :
    int m_res;
    int m_numRenderTargets;
    float m_AABCLength;
    float m_voxelLength;
    glm::vec3 m_meshCenter;
    glm::vec3 m_origBBox;
    GLVoxelFramebuffer m_frameBuffer;

    GLuint m_VoxelGridTextureObject;

public:
    GLVoxelizer();

    ~GLVoxelizer();

    /**
     * @brief voxelize Voxelize a mesh
     * @param resolution The resolution for the voxel grid
     * @param mesh The mesh
     *
     * This function affects the following states of OpenGL:
     * - GL_CURRENT_PROGRAM
     * - GL_VIEWPORT
     * - GL_DEPTH_TEST
     * - GL_CULL_FACE
     * - GL_COLOR_LOGIC_OP
     * - GL_LOGIC_OP_MODE
     */
    VoxelSpace voxelize(int resolution, const Mesh &mesh);

    BnZ::VoxelGrid fillVoxelGrid();

    CubicalComplex createCubicalComplex();

    float getVoxelLength() const {
        return m_voxelLength;
    }

    glm::vec3 getOrigBBox() const {
        return m_origBBox;
    }

    glm::mat4 getGridToWorldMatrix() const {
        return glm::scale(glm::translate(glm::mat4(1.f), m_origBBox), glm::vec3(m_voxelLength));
    }

};

#endif
