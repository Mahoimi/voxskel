#ifndef VOXELSPACE_HPP
#define VOXELSPACE_HPP

#include <cstdint>
#include <glm/glm.hpp>

struct VoxelSpace {
    // Transformation matrices
    glm::mat4 gridToWorld, worldToGrid;
    // Resolution of the grid
    uint32_t resolution;
};

#endif // VOXELSPACE_HPP
