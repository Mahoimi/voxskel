#ifndef _GLVOXELFRAMEBUFFER_H_
#define _GLVOXELFRAMEBUFFER_H_

#include <GL/glew.h>

// An OpenGL framebuffer to store a voxelization
class GLVoxelFramebuffer {
private:
    GLuint m_FBO;
    GLuint* m_renderedTexture;
    int m_width;
    int m_height;
    int m_numTextures;

    // Can't be copied
    GLVoxelFramebuffer(const GLVoxelFramebuffer&) = delete;
    const GLVoxelFramebuffer& operator =(const GLVoxelFramebuffer&) = delete;

public:
    GLVoxelFramebuffer();

    ~GLVoxelFramebuffer();

    bool init(int m_width,int m_height, int m_numTextures);

    void bind(GLenum e) const{
        glBindFramebuffer(e, m_FBO);
    }

    GLuint getRenderedTexture(int i) const {
        return m_renderedTexture[i];
    }
};

#endif
