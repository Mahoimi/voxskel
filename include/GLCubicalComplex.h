#ifndef GLCUBICALCOMPLEX_H
#define GLCUBICALCOMPLEX_H

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "CubicalComplex.h"

class GLCubicalComplex {

public:
    GLuint m_VBO;
    GLuint m_VAO;
    size_t m_nVertexCount;
    GLuint m_CCTextureObject;

public:
    struct CCVertex{
        glm::ivec3 m_position;

        CCVertex(glm::ivec3 position):m_position(position) {}
    };

    GLuint getTextureObject() const {
        return m_CCTextureObject;
    }

    GLCubicalComplex(): m_nVertexCount(0) {
        glGenBuffers(1, &m_VBO);
        glGenVertexArrays(1, &m_VAO);
        glGenTextures(1, &m_CCTextureObject);
    }

    ~GLCubicalComplex() {
        glDeleteBuffers(1, &m_VBO);
        glDeleteVertexArrays(1, &m_VAO);
        glDeleteTextures(1, &m_CCTextureObject);
    }

    void init(const CubicalComplex& cubicalComplex);

    void render() const {
        glBindVertexArray(m_VAO);
        glDrawArrays(GL_POINTS, 0, m_nVertexCount);
    }
};


#endif // GLCUBICALCOMPLEX_H
