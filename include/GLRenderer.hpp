#ifndef GLRENDERER_HPP
#define GLRENDERER_HPP

#include "GLProgram.h"
#include "GLScene.h"
#include "GLCubicalComplex.h"

class GLRenderer {
public:
    void setProjectionMatrix(glm::mat4 projectionMatrix) {
        m_ProjectionMatrix = projectionMatrix;
    }

    void setViewMatrix(glm::mat4 viewMatrix) {
        m_ViewMatrix = viewMatrix;
    }

    void drawScene(const GLScene& scene);

    void drawCubicalComplex(const glm::mat4& gridToWorld,
                            const GLCubicalComplex& cubicalComplex);

private:
    glm::mat4 m_ProjectionMatrix;
    glm::mat4 m_ViewMatrix;

    struct RenderScenePass {
        GLProgram m_Program;

        GLint m_uMVLocation;
        GLint m_uMVPLocation;

        RenderScenePass();
    };
    RenderScenePass m_RenderScenePass;

    struct RenderCubicalComplexFacesPass {
        GLProgram m_Program;

        GLint m_uMVLocation;
        GLint m_uMVPLocation;
        GLint m_uCubicalComplexLocation;

        RenderCubicalComplexFacesPass();
    };
    RenderCubicalComplexFacesPass m_RenderCubicalComplexFacesPass;

    struct RenderCubicalComplexEdgesPass {
        GLProgram m_Program;

        GLint m_uMVLocation;
        GLint m_uMVPLocation;
        GLint m_uCubicalComplexLocation;

        RenderCubicalComplexEdgesPass();
    };
    RenderCubicalComplexEdgesPass m_RenderCubicalComplexEdgesPass;

    struct RenderCubicalComplexPointsPass {
        GLProgram m_Program;

        GLint m_uMVLocation;
        GLint m_uMVPLocation;
        GLint m_uCubicalComplexLocation;

        RenderCubicalComplexPointsPass();
    };
    RenderCubicalComplexPointsPass m_RenderCubicalComplexPointsPass;
};

#endif // GLSCENERENDERER_HPP
