#ifndef _MESH_H_
#define _MESH_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>

#include "Vertex.h"

class aiScene;
class aiMesh;

class Mesh {
private:
    struct MeshEntry {
        MeshEntry();

        ~MeshEntry();

        MeshEntry(const MeshEntry&) = delete;

        MeshEntry& operator =(const MeshEntry&) = delete;

        MeshEntry(MeshEntry&& rvalue);

        MeshEntry& operator =(MeshEntry&& rvalue);

        void init(const std::vector<Vertex>& Vertices,
                  const std::vector<unsigned int>& Indices);

        GLuint VB;
        GLuint IB;
        GLuint VAO;
        unsigned int NumIndices;
    };
/*
    struct AABBModel {
        AABBModel();

        ~AABBModel();

        void init(float AABBLength, glm::vec3 center);
        void draw() const;

        GLuint VBO;
        GLuint VAO;
    };
    
    AABBModel m_AABBModel;*/
    std::vector<MeshEntry> m_Entries;
    float m_AABCLength;
    glm::vec3 m_center;

    glm::vec3 m_BBoxMin, m_BBoxMax;

    bool initFromScene(const aiScene* pScene, const std::string& Filename);
    void initMesh(unsigned int Index, const aiMesh* paiMesh);

    Mesh(const Mesh&) = delete;

    const Mesh& operator =(const Mesh&) = delete;

public:
    Mesh() {}

    bool load(const std::string& Filename);

    void render() const;

    inline int getEntrySize() const {
        return m_Entries.size();
    }

    inline float getAABCLength() const {
        return m_AABCLength;
    }

    inline glm::vec3 getCenter() const {
        return m_center;
    }
};


#endif
