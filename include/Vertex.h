#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <GL/glew.h>
#include <glm/glm.hpp>

struct Vertex {
    glm::vec3 position, normal;
    glm::vec2 texCoords;
};

#endif