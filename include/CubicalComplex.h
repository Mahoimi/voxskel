#ifndef __CUBICALCOMPLEX_H__
#define __CUBICALCOMPLEX_H__

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>

enum facet_data {
    NOFACET = 0,
    POINT = 1 << 0,
    XEDGE = 1 << 1,
    YEDGE = 1 << 2,
    ZEDGE = 1 << 3,
    XYFACE = 1 << 4,
    YZFACE = 1 << 5,
    XZFACE = 1 << 6,
    CUBE = 1 << 7,
    ALL = POINT | XEDGE | YEDGE | ZEDGE | XYFACE | YZFACE | XZFACE | CUBE
};

class CubicalComplex {
    friend class CudaCubicalComplex;
public:
    typedef int8_t CCElmtType;

    struct Facet {
        CCElmtType m_data;

        Facet():m_data(NOFACET) {}

        explicit Facet(facet_data f):m_data(f) {}

        operator CCElmtType() const {
            return m_data;
        }

        void fill(){
            m_data = ALL;
        }

        void add(CCElmtType d) {
            m_data |= d;
        }

        void remove(CCElmtType d) {
            m_data = m_data&(~d);
        }

        bool has(CCElmtType d) const {
            return m_data & d;
        }

        bool exists() const {
            return m_data != NOFACET;
        }
    };
private:
    std::vector<Facet> m_faces;
    std::vector<bool> m_isInBorder;
    std::vector<glm::ivec3> m_borders[6];
    std::size_t m_width, m_height, m_depth;

    void collapseFreeFaces(int indexBorder, int indexElement, int direction, int orientation, int dimension);

    void cudaCalculateFirstBorder();

    void testForBorder(int x, int y, int z);
    void clearBorder();

    void updateBorder();
    void calculateUpdatedBorder(const std::vector<glm::ivec3> &updatedBorder);
    void clearBoolBorder(const std::vector<glm::ivec3> &updatedBorder);

    void updateBorderXNEG(std::vector<glm::ivec3>& updatedBorder);
    void updateBorderXPOS(std::vector<glm::ivec3>& updatedBorder);
    void updateBorderYNEG(std::vector<glm::ivec3>& updatedBorder);
    void updateBorderYPOS(std::vector<glm::ivec3>& updatedBorder);
    void updateBorderZNEG(std::vector<glm::ivec3>& updatedBorder);
    void updateBorderZPOS(std::vector<glm::ivec3>& updatedBorder);

    bool isFreeXNEG(int x, int y, int z) const;
    bool isFreeXNEG3D(int x, int y, int z) const;
    bool isFreeXNEG2D(int x, int y, int z) const;
    bool isFreeXNEG_XYFACE(int x, int y, int z) const;
    bool isFreeXNEG_XZFACE(int x, int y, int z) const;
    bool isFreeXNEG1D(int x, int y, int z) const;

    bool isFreeXPOS(int x, int y, int z) const;
    bool isFreeXPOS3D(int x, int y, int z) const;
    bool isFreeXPOS2D(int x, int y, int z) const;
    bool isFreeXPOS_XYFACE(int x, int y, int z) const;
    bool isFreeXPOS_XZFACE(int x, int y, int z) const;
    bool isFreeXPOS1D(int x, int y, int z) const;

    bool isFreeYNEG(int x, int y, int z) const;
    bool isFreeYNEG3D(int x, int y, int z) const;
    bool isFreeYNEG2D(int x, int y, int z) const;
    bool isFreeYNEG_XYFACE(int x, int y, int z) const;
    bool isFreeYNEG_YZFACE(int x, int y, int z) const;
    bool isFreeYNEG1D(int x, int y, int z) const;

    bool isFreeYPOS(int x, int y, int z) const;
    bool isFreeYPOS3D(int x, int y, int z) const;
    bool isFreeYPOS2D(int x, int y, int z) const;
    bool isFreeYPOS_XYFACE(int x, int y, int z) const;
    bool isFreeYPOS_YZFACE(int x, int y, int z) const;
    bool isFreeYPOS1D(int x, int y, int z) const;

    bool isFreeZNEG(int x, int y, int z) const;
    bool isFreeZNEG3D(int x, int y, int z) const;
    bool isFreeZNEG2D(int x, int y, int z) const;
    bool isFreeZNEG_XZFACE(int x, int y, int z) const;
    bool isFreeZNEG_YZFACE(int x, int y, int z) const;
    bool isFreeZNEG1D(int x, int y, int z) const;

    bool isFreeZPOS(int x, int y, int z) const;
    bool isFreeZPOS3D(int x, int y, int z) const;
    bool isFreeZPOS2D(int x, int y, int z) const;
    bool isFreeZPOS_XZFACE(int x, int y, int z) const;
    bool isFreeZPOS_YZFACE(int x, int y, int z) const;
    bool isFreeZPOS1D(int x, int y, int z) const;

    void setInBorder(int x, int y, int z, bool b = true){
        m_isInBorder[x + y * m_width + z * m_width * m_height] = b;
    }

    bool isInBorder(int x, int y, int z) const{
       return m_isInBorder[x + y * m_width + z * m_width * m_height];
    }

    bool stillFreeFaces(){
        return !m_borders[0].empty() || !m_borders[1].empty() ||
               !m_borders[2].empty() || !m_borders[3].empty() ||
               !m_borders[4].empty() || !m_borders[5].empty();
    }

    bool isInGrid(int x, int y, int z) const {
        return !(x < 0 || y < 0 || z < 0 || x >= m_width || y >= m_height || z >= m_depth);
    }

    int index(int x, int y, int z) const {
        return x + y * m_width + z * m_width * m_height;
    }

    uint32_t recursiveDepthFirstTraversal(int x, int y, int z, std::vector<bool>& visited) const;

public:
    CubicalComplex(){}
    explicit CubicalComplex(std::size_t res) : m_width(res+1),m_height(res+1),m_depth(res+1){
        m_faces=std::vector<Facet>(m_width * m_height * m_depth,Facet());
        m_isInBorder = std::vector<bool>(m_width * m_height * m_depth,false);
    }

    Facet operator ()(int x, int y, int z) const{
        return m_faces[x + y * m_width + z * m_width * m_height];
    }

    Facet& operator ()(int x, int y, int z) {
        return m_faces[x + y * m_width + z * m_width * m_height];
    }

    Facet operator ()(glm::ivec3 pos) const{
        return m_faces[pos.x + pos.y * m_width + pos.z * m_width * m_height];
    }

    Facet& operator ()(glm::ivec3 pos) {
        return m_faces[pos.x + pos.y * m_width + pos.z * m_width * m_height];
    }

    std::size_t width() const{
        return m_width;
    }

    std::size_t height() const{
        return m_height;
    }

    std::size_t depth() const{
        return m_depth;
    }

    void collapseFreePairsXNeg3D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsXNeg2D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsXNeg1D(const std::vector<glm::ivec3>& border);

    void collapseFreePairsXPos3D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsXPos2D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsXPos1D(const std::vector<glm::ivec3>& border);

    void collapseFreePairsYNeg3D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsYNeg2D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsYNeg1D(const std::vector<glm::ivec3>& border);

    void collapseFreePairsYPos3D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsYPos2D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsYPos1D(const std::vector<glm::ivec3>& border);

    void collapseFreePairsZNeg3D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsZNeg2D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsZNeg1D(const std::vector<glm::ivec3>& border);

    void collapseFreePairsZPos3D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsZPos2D(const std::vector<glm::ivec3>& border);
    void collapseFreePairsZPos1D(const std::vector<glm::ivec3>& border);

    void calculateFirstBorder();
    void parDirCollapse(int iter);
    void parDirCollapse();

    uint32_t computeConnexityNumber() const;

    int computeEulerCharacteristic() const;

    const std::vector<Facet>& getFaces() const {
        return m_faces;
    }
};

#endif
