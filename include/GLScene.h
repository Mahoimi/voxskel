#ifndef _GLSCENE_H_
#define _GLSCENE_H_

#include <vector>
#include <GL/glew.h>
#include "VoxelGrid.hpp"
#include "GLProgram.h"
#include "FreeFlyCamera.h"
#include "MatrixStack.h"
#include "Mesh.h"
#include "GLVoxelizer.h"
#include "CubicalComplex.h"
#include "GLCubicalComplex.h"

class GLScene {

    public:
        Mesh m_mesh;

        GLVoxelizer m_voxelizer;

        CubicalComplex m_cubicalComplex;
        GLCubicalComplex m_glCubicComplex;

        void initVoxelization();
        void initForRendering();

        void createCubicalComplex();
        void createGLCubicalComplex();
        void createVoxelGrid();

    public:
        GLScene();
        void display();
        void skeletonization();
        void skeletonization(int iter);

        void draw() const {
            m_mesh.render();
        }
};

#endif
