#ifndef _WINDOWMANAGER_H_
#define _WINDOWMANAGER_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "GLScene.h"
#include "GLRenderer.hpp"

class WindowManager{
	private:
        GLFWwindow* m_pWindow;
		void shutDown();

    public:
		WindowManager(int width, int height);
		void draw();
        void draw(GLRenderer& renderer, GLScene& scene, FreeFlyCamera& camera);

        ~WindowManager();
};

#endif
