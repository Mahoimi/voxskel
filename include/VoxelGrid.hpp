#ifndef _BONEZ_VOXELGRID_HPP_
#define _BONEZ_VOXELGRID_HPP_

#include <cstdint>
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <GL/glew.h>

namespace BnZ {

template<typename T>
class Grid3D: std::vector<T> {
    typedef std::vector<T> Base;
public:
    typedef typename Base::value_type value_type;
    typedef typename Base::reference reference;
    typedef typename Base::iterator iterator;
    typedef typename Base::const_iterator const_iterator;
    using Base::data;
    using Base::size;
    using Base::empty;
    using Base::begin;
    using Base::end;
    using Base::operator [];

    Grid3D() {
    }

    Grid3D(size_t width, size_t height, size_t depth):
        Base(width * height * depth),
        m_nWidth(width),
        m_nHeight(height),
        m_nDepth(depth) {
    }

    uint32_t offset(uint32_t x, uint32_t y, uint32_t z) const {
        return x + y * m_nWidth + z * m_nWidth * m_nHeight;
    }

    value_type operator ()(uint32_t x, uint32_t y, uint32_t z) const {
        return (*this)[offset(x, y, z)];
    }

    reference operator ()(uint32_t x, uint32_t y, uint32_t z) {
        return (*this)[offset(x, y, z)];
    }

    bool contains(int x, int y, int z) const {
        return x >= 0 &&
                y >= 0 &&
                z >= 0 &&
                x < (int)m_nWidth &&
                y < (int)m_nHeight &&
                z < (int)m_nDepth;
    }

    size_t width() const {
        return m_nWidth;
    }

    size_t height() const {
        return m_nHeight;
    }

    size_t depth() const {
        return m_nDepth;
    }

private:
    size_t m_nWidth, m_nHeight, m_nDepth;
};

// 0 = no voxel, 1 = voxel
typedef Grid3D<uint8_t> VoxelGrid;

class GLGrid {
public:
    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;

        Vertex(glm::vec3 position, glm::vec3 normal):
            position(position), normal(normal) {
        }
    };

    GLGrid(): m_nVertexCount(0) {
        glGenBuffers(1, &m_VBO);
        glGenVertexArrays(1, &m_VAO);
    }
    
    ~GLGrid() {
        glDeleteBuffers(1, &m_VBO);
        glDeleteVertexArrays(1, &m_VAO);
    }

    void init(const VoxelGrid& grid, float voxelLength, const glm::vec3& origBBox);

    void render() const;

private:
    GLuint m_VBO;
    GLuint m_VAO;
    size_t m_nVertexCount;
};

#define V1 glm::vec3(0, 0, 0)
#define V2 glm::vec3(1, 0, 0)
#define V3 glm::vec3(1, 1, 0)
#define V4 glm::vec3(0, 1, 0)
#define V5 glm::vec3(0, 0, 1)
#define V6 glm::vec3(0, 1, 1)
#define V7 glm::vec3(1, 1, 1)
#define V8 glm::vec3(1, 0, 1)

#define N0 glm::vec3(0, 0, -1)
#define N1 glm::vec3(0, 1, 0)
#define N2 glm::vec3(-1, 0, 0)
#define N3 glm::vec3(0, -1, 0)
#define N4 glm::vec3(1, 0, 0)
#define N5 glm::vec3(0, 0, 1)

inline glm::vec3 transformPoint(const glm::mat4& transform, const glm::vec3& point) {
    return glm::vec3(transform * glm::vec4(point, 1.f));
}

inline glm::vec3 transformVector(const glm::mat4& transform, const glm::vec3& vector) {
    return glm::vec3(transform * glm::vec4(vector, 0.f));
}

inline void GLGrid::init(const VoxelGrid& grid, float voxelLength, const glm::vec3& origBBox) {
    int size_x = grid.width();
    int size_y = grid.height();
    int size_z = grid.depth();

    std::vector<Vertex> vertices;

    glm::vec3 P[] = {
        V1, V2, V3, V4, V5, V6, V7, V8
    };

    for(int i = 0; i < 8; ++i) {
        P[i] -= glm::vec3(0.5f);
        P[i] *= voxelLength;
    }

    const glm::vec3 N[] = {
        N0,
        N1,
        N2,
        N3,
        N4,
        N5
    };

    const uint32_t F[6][6] = {
        { 0, 3, 2, 0, 2, 1 },
        { 5, 6, 2, 5, 2, 3 },
        { 0, 4, 5, 0, 5, 3 },
        { 0, 7, 4, 0, 1, 7 },
        { 7, 1, 2, 7, 2, 6 },
        { 4, 7, 6, 4, 6, 5 }
    };

#define ADD_FACE(voxel, face) \
    vertices.emplace_back(voxel + P[F[face][0]], N[face]); \
    vertices.emplace_back(voxel + P[F[face][1]], N[face]); \
    vertices.emplace_back(voxel + P[F[face][2]], N[face]); \
    vertices.emplace_back(voxel + P[F[face][3]], N[face]); \
    vertices.emplace_back(voxel + P[F[face][4]], N[face]); \
    vertices.emplace_back(voxel + P[F[face][5]], N[face]); \

    for(int z = 0; z < size_z; ++z) {
        for(int y = 0; y < size_y; ++y) {
            for(int x = 0; x < size_x; ++x) {
                if(grid(x, y, z)) {
                    glm::vec3 voxel(origBBox.x + (x+0.5) * voxelLength,
                                    origBBox.y + (y+0.5) * voxelLength,
                                    origBBox.z + (z+0.5) * voxelLength);
                    if(x == 0 || !grid(x - 1, y, z)) {
                        ADD_FACE(voxel, 2);
                    }

                    if(y == 0 || !grid(x, y - 1, z)) {
                        ADD_FACE(voxel, 3);
                    }

                    if(z == 0 || !grid(x, y, z - 1)) {
                        ADD_FACE(voxel, 0);
                    }

                    if(x == size_x - 1 || !grid(x + 1, y, z)) {
                        ADD_FACE(voxel, 4);
                    }

                    if(y == size_y - 1 || !grid(x, y + 1, z)) {
                        ADD_FACE(voxel, 1);
                    }


                    if(z == size_z - 1 || !grid(x, y, z + 1)) {
                        ADD_FACE(voxel, 5);
                    }
                }
            }
        }
    }

    m_nVertexCount = vertices.size();

    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

    glBindVertexArray(m_VAO);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) offsetof(Vertex, position));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) offsetof(Vertex, normal));
#undef ADD_FACE
}

inline void GLGrid::render() const {
    glBindVertexArray(m_VAO);
    glDrawArrays(GL_TRIANGLES, 0, m_nVertexCount);
}

}

#endif
