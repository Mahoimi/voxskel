#ifndef __VOXELBUFFER_H__
#define __VOXELBUFFER_H__

#include <vector>
#include <glm/glm.hpp>

class VoxelBuffer {
private:
    std::vector<glm::ivec4> m_data;
    std::size_t m_res, m_numRenderTarget;
    int m_bitmask[32];

public:
    VoxelBuffer();
    VoxelBuffer(std::size_t res, std::size_t numRenderTarget) : m_res(res), m_numRenderTarget(numRenderTarget) {
        m_data = std::vector<glm::ivec4>(res * res * numRenderTarget, glm::ivec4(0));
        for (int i = 0; i < 32; ++i){
            m_bitmask[i] = 1 << i;
        }
    }

    void storeData() {
        for(std::size_t i = 0; i < m_numRenderTarget; ++i) {
            glReadBuffer(GL_COLOR_ATTACHMENT0 + i);
            glReadPixels(0, 0, m_res, m_res, GL_RGBA_INTEGER, GL_INT, m_data.data() + m_res * m_res * i);
        }
    }

    bool operator()(int x, int y, int z) {
        int colorBuffer = z / 128;
        int absoluteBitPosition = z % 128;
        int ivecPosition = absoluteBitPosition / 32;
        int bitPosition = absoluteBitPosition % 32;

        return ((m_data[x + y * m_res + colorBuffer * m_res * m_res][ivecPosition] & m_bitmask[bitPosition]) !=0);
    }

    template<typename Functor>
    void getVoxels(Functor f) {
        for(std::size_t texture = 0; texture < m_numRenderTarget; ++texture) {
            glm::ivec4* pData = m_data.data() + texture * m_res * m_res;
            //Convert data in voxel position
            for (std::size_t i = 0; i < m_res * m_res; i++) {
                int x = i % m_res;
                int y = (i - x) / m_res;
                for(std::size_t j = 0; j < 4 && j * 32 < m_res; j++){
                    for(std::size_t k = 0; k < 32 && k < m_res; k++) {
                        int z = texture * 128 + j * 32 + k;
                        if ((pData[i][j] & m_bitmask[k]) != 0) {
                            f(x, y, z);
                        }
                    }
                }
            }
        }
    }

    template<typename Functor>
    void getEmptySpace(Functor f) {
        for(std::size_t texture = 0; texture < m_numRenderTarget; ++texture) {
            glm::ivec4* pData = m_data.data() + texture * m_res * m_res;
            //Convert data in voxel position
            for (std::size_t i = 0; i < m_res * m_res; i++) {
                int x = i % m_res;
                int y = (i - x) / m_res;
                for(std::size_t j = 0; j < 4 && j * 32 < m_res; j++){
                    for(std::size_t k = 0; k < 32 && k < m_res; k++) {
                        int z = texture * 128 + j * 32 + k;
                        if ((pData[i][j] & m_bitmask[k]) == 0) {
                            f(x, y, z);
                        }
                    }
                }
            }
        }
    }

};

#endif
